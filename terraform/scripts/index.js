async function handleRequest(request) {
  // Return a new Response based on a URL's hostname
  var url = new URL(request.url)
  const userAgent = (request.headers.get("User-Agent") || "").toLowerCase()

  var init = {
    headers: request.headers,
  }

  if (url.pathname == "/") {
    console.log(`url.pathname: ${url.pathname}`)
    var userAgentScripts = {
      "curl": "https://dotnetzero.com/init.sh",
      "wget": "https://dotnetzero.com/init.sh",
      "powershell": "https://dotnetzero.com/init.ps1",
      "pwsh": "https://dotnetzero.com/init.ps1"
    }
    for (var key in userAgentScripts) {
      if (userAgent.includes(key)) {
        console.log(`${key}:${userAgentScripts[key]}`);
        url = new URL(userAgentScripts[key])
        continue
      }
    }
  }

  console.log(`Requesting ${url} with headers: ${JSON.stringify(init)}`)
  return await fetch(url, init)
}

addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request))
})
