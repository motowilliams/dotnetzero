task Clean-Packages {
    Remove-Item -Force -Recurse $packagesDirectory;
    CreateDirectory $packagesDirectory;
}

task Clean {
    DeleteDirectory $outputDirectory\**

    @("bin","obj") | ForEach-Object {
        DeleteDirectory "$sourceDirectory\**\$_\"
    }
}

task Default -depends Clean, Init, Compile, Unit-Test

task Init {
    Assert(Test-Path $nugetExe) -failureMessage "Nuget command line tool is missing at $nugetExe"

    Write-Host "Creating build output directory at $outputDirectory"
    CreateDirectory $outputDirectory
}

task Compile -depends Init,Restore-Packages,Create-AssemblyInfo {
    Exec {
        msbuild $solutionFile `
            /verbosity:$verbosity `
            /m `
            /nr:false `
            /p:Configuration=$buildConfiguration `
            /p:Platform=$buildPlatform `
            /p:OctoPackPackageVersion=$version `
            /p:RunOctoPack=$runOctoPack `
            /p:OctoPackEnforceAddingFiles=true `
            /p:OctoPackPublishPackageToFileShare="$packagesOutputDirectory"
    }
}

task Compile -depends Init, Restore-Packages, Create-AssemblyInfo {
    Exec {
        msbuild $solutionFile `
            /verbosity:$verbosity `
            /m `
            /nr:false `
            /p:Configuration=$buildConfiguration `
            /p:Platform=$buildPlatform
    }
}

task Rebuild-Database -depends Compile {

}

task Restore-Packages {
    Exec { & $nugetExe "restore" $solutionFile }
}

TaskSetup{
    if($env:TEAMCITY_VERSION){
        Write-Output "##teamcity[blockOpened name='$taskName']"
    }
}

TaskTearDown{
    if($env:TEAMCITY_VERSION){
        Write-Output "##teamcity[blockClosed name=$taskName']"
    }
}

task Unit-Test -depends Compile {

}

